"""Serializers for the example app."""

from rest_framework import serializers

from drf_serializer_query_optimizations.serializers import (
    QueryOptimizingListSerializer
)

from . import models


class EmployeeSerializer(serializers.ModelSerializer):
    """A serializer for employees."""

    class Meta:
        """Meta class for the serializer."""

        model = models.Employee
        fields = [
            'first_name',
            'last_name',
        ]


class DepartmentWithEmployeesSerializer(serializers.ModelSerializer):
    """A serializer for departments with employees."""

    employees = EmployeeSerializer(many=True)

    class Meta:
        """Meta class for the serializer."""

        model = models.Department
        fields = [
            'name',
            'employees',
        ]


class CompanyWithDepartmentsSerializer(serializers.ModelSerializer):
    """A regular serializer for companies with departments."""

    departments = DepartmentWithEmployeesSerializer(many=True)

    class Meta:
        """Meta class for the serializer."""

        model = models.Company
        fields = [
            'name',
            'departments',
        ]


class OptimizedCompanyWithDepartmentsSerializer(
    CompanyWithDepartmentsSerializer
):
    """A serializer for companies using the optimized list serializer."""

    class Meta(CompanyWithDepartmentsSerializer.Meta):
        """Meta class for the serializer."""

        prefetch_related = [
            'departments__employees',
        ]
        list_serializer_class = QueryOptimizingListSerializer


class CompanySerializer(serializers.ModelSerializer):
    """A simple serializer for companies."""

    class Meta:
        """Meta class for the serializer."""

        model = models.Company
        fields = [
            'name',
        ]


class DepartmentWithCompanySerializer(serializers.ModelSerializer):
    """A serializer for departments with companies."""

    company = CompanySerializer()

    class Meta:
        """Meta class for the serializer."""

        model = models.Department
        fields = [
            'name',
            'company',
        ]


class EmployeeWithDepartmentSerializer(serializers.ModelSerializer):
    """A regular serializer for employees with departments."""

    department = DepartmentWithCompanySerializer()

    class Meta:
        """Meta class for the serializer."""

        model = models.Employee
        fields = [
            'first_name',
            'last_name',
            'department',
        ]


class OptimizedEmployeeWithDepartmentSerializer(
    EmployeeWithDepartmentSerializer
):
    """A serializer for departments using the optimized list serializer."""

    class Meta(EmployeeWithDepartmentSerializer.Meta):
        """Meta class for the serializer."""

        select_related = [
            'department__company',
        ]
        list_serializer_class = QueryOptimizingListSerializer
