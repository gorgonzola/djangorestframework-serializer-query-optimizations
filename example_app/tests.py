from django.db import connection
from django.test import TestCase

from . import models
from . import serializers


class BaseTestCase(TestCase):
    """A base test case setting up some data."""

    def setUp(self):
        """Set up some test data."""

        self.company1 = models.Company.objects.create(
            name='Company 1'
        )

        self.company2 = models.Company.objects.create(
            name='Company 2'
        )

        self.company3 = models.Company.objects.create(
            name='Company 3'
        )

        self.department1 = models.Department.objects.create(
            company=self.company1,
            name='Department 1'
        )

        self.department2 = models.Department.objects.create(
            company=self.company1,
            name='Department 2'
        )

        self.department3 = models.Department.objects.create(
            company=self.company1,
            name='Department 3'
        )

        self.department4 = models.Department.objects.create(
            company=self.company2,
            name='Department 4'
        )

        self.department5 = models.Department.objects.create(
            company=self.company2,
            name='Department 5'
        )

        self.department6 = models.Department.objects.create(
            company=self.company3,
            name='Department 6'
        )

        for department in models.Department.objects.all():
            employee_count = int(department.name.split(' ')[-1])
            for i in range(employee_count):
                models.Employee.objects.create(
                    department=department,
                    first_name='John',
                    last_name='Johnson'
                )

    def serializer_key_test(self, serializer_class, instance, expected_keys):
        """Test the data keys of a serializer."""
        serializer = serializer_class(instance)
        self.assertEqual(len(expected_keys), len(serializer.data.keys()))
        for key in expected_keys:
            self.assertIn(key, serializer.data.keys())


class EmployeeSerializerTestCase(BaseTestCase):
    """Tests for the simple employee serializer."""

    def test_keys(self):
        """Test the data keys of the serializer."""
        self.serializer_key_test(
            serializers.EmployeeSerializer,
            models.Employee.objects.first(),
            [
                'first_name',
                'last_name',
            ]
        )


class DepartmentWithEmployeesSerializerTestCase(BaseTestCase):
    """Tests for the department with employees serializer."""

    def test_keys(self):
        """Test the data keys of the serializer."""
        self.serializer_key_test(
            serializers.DepartmentWithEmployeesSerializer,
            models.Department.objects.first(),
            [
                'name',
                'employees',
            ]
        )

    def test_employees(self):
        """Test that employees is a list of non-ints with a correct length."""
        serializer = serializers.DepartmentWithEmployeesSerializer(
            self.department5
        )
        self.assertTrue(isinstance(serializer.data.get('employees'), list))
        self.assertEqual(
            self.department5.employees.count(),
            len(serializer.data.get('employees'))
        )
        self.assertFalse(isinstance(serializer.data.get('employees')[0], int))


class CompanyWithDepartmentsSerializerTestCase(BaseTestCase):
    """Tests for the company with departments serializer."""

    serializer = serializers.CompanyWithDepartmentsSerializer

    def test_keys(self):
        """Test the data keys of the serializer."""
        self.serializer_key_test(
            self.serializer,
            models.Company.objects.first(),
            [
                'name',
                'departments',
            ]
        )

    def test_departments(self):
        """Test that departments is a list of non-ints with a  correct length."""
        serializer = self.serializer(
            self.company1
        )
        self.assertTrue(isinstance(serializer.data.get('departments'), list))
        self.assertEqual(
            self.company1.departments.count(),
            len(serializer.data.get('departments'))
        )
        self.assertFalse(
            isinstance(serializer.data.get('departments')[0],
            int)
        )


class OptimizedCompanyWithDepartmentsSerializerTestCase(
    CompanyWithDepartmentsSerializerTestCase
):
    """Tests for the optimized company serializer."""

    serializer = serializers.OptimizedCompanyWithDepartmentsSerializer


class CompanyListSerializerTestCase(BaseTestCase):
    """
    Tests for serialized lists of companies.

    This is where things get interesting, as we have a company serializer using
    the query optimizing list serializer.
    """

    def test_regular_serializer(self):
        expected_amount_of_queries = (
            1 + # The company listing.
            models.Company.objects.count() + # Department listing per company.
            models.Department.objects.count() # Employee listing per dept.
        )
        with self.assertNumQueries(expected_amount_of_queries):
            serializer = serializers.CompanyWithDepartmentsSerializer(
                models.Company.objects.all(),
                many=True
            )
            serializer.data

    def test_prefech_related_serializer(self):
        expected_amount_of_queries = 3 # 1 per involved model.
        with self.assertNumQueries(expected_amount_of_queries):
            serializer = serializers.OptimizedCompanyWithDepartmentsSerializer(
                models.Company.objects.all(),
                many=True
            )
            serializer.data


# Above: Tests for serializers related to `prefetch_related`.

# Below: Tests for serializers related to `select_related`.


class CompanySerializerTestCase(BaseTestCase):
    """Tests for the simple company serializer."""

    def test_keys(self):
        """Test the data keys of the serializer."""
        self.serializer_key_test(
            serializers.CompanySerializer,
            models.Company.objects.first(),
            [
                'name',
            ]
        )


class DepartmentWithCompanySerializerTestCase(BaseTestCase):
    """Tests for the department with company serializer."""

    def test_keys(self):
        """Test the data keys of the serializer."""
        self.serializer_key_test(
            serializers.DepartmentWithCompanySerializer,
            models.Department.objects.first(),
            [
                'name',
                'company',
            ]
        )

    def test_company(self):
        """Test that the company looks right."""
        serializer = serializers.DepartmentWithCompanySerializer(
            self.department5
        )
        self.assertFalse(isinstance(serializer.data.get('company'), list))
        self.assertEqual(
            self.department5.company.name,
            serializer.data.get('company').get('name')
        )
        self.assertFalse(isinstance(serializer.data.get('company'), int))


class EmployeeWithDepartmentSerializerTestCase(BaseTestCase):
    """Tests for the employee with departments serializer."""

    serializer = serializers.EmployeeWithDepartmentSerializer

    def test_keys(self):
        """Test the data keys of the serializer."""
        self.serializer_key_test(
            self.serializer,
            models.Employee.objects.first(),
            [
                'first_name',
                'last_name',
                'department',
            ]
        )

    def test_department(self):
        """Test that the department looks right."""
        instance = models.Employee.objects.first()
        serializer = self.serializer(instance)
        self.assertFalse(isinstance(serializer.data.get('department'), list))
        self.assertEqual(
            instance.department.name,
            serializer.data.get('department').get('name')
        )
        self.assertFalse(isinstance(serializer.data.get('department'), int))


class OptimizedEmployeeWithDepartmentSerializerTestCase(
    EmployeeWithDepartmentSerializerTestCase
):
    """Tests for the optimized employee serializer."""

    serializer = serializers.OptimizedEmployeeWithDepartmentSerializer


class EmployeeListSerializerTestCase(BaseTestCase):
    """
    Tests for serialized lists of employees.

    This is where things get interesting, as we have an employee serializer
    using the query optimizing list serializer.
    """

    def test_regular_serializer(self):
        expected_amount_of_queries = (
            1 + # The employee listing.
            models.Employee.objects.count() * 2 # 1 for dept., 1 for company.
        )
        with self.assertNumQueries(expected_amount_of_queries):
            serializer = serializers.EmployeeWithDepartmentSerializer(
                models.Employee.objects.all(),
                many=True
            )
            serializer.data

    def test_select_related_serializer(self):
        expected_amount_of_queries = 1 # 1 giant query.
        with self.assertNumQueries(expected_amount_of_queries):
            serializer = (
                serializers.OptimizedEmployeeWithDepartmentSerializer(
                    models.Employee.objects.all(),
                    many=True
                )
            )
            serializer.data
