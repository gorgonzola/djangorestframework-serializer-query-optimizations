"""Models for the example app."""

from django.db import models


class Company(models.Model):
    """A company."""

    name = models.CharField(max_length=100)


class Department(models.Model):
    """A department is a subdivision of a company."""

    company = models.ForeignKey(Company, related_name='departments')
    name = models.CharField(max_length=100)


class Employee(models.Model):
    """An employee works in a department of a company."""

    department = models.ForeignKey(Department, related_name='employees')
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
