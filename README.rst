djangorestframework-serializer-query-optimizations
==================================================

Overview
--------

Adds the possibility to do ``select_related()`` and ``prefetch_related()`` queries on querysets provided to Django REST Framework (DRF) model serializers.

Currently, the features are only supported on list serializers. The focus has been here, because non-optimized list queries are those that generate the most database queries, resulting in the longest response times. Adding support for single instance serializers might come some day.

Requirements
------------

-  Python (2.7, 3)
-  Django (1.10+)
-  Django REST Framework (3.6+)

Installation
------------

Install using ``pip``:

.. code:: bash

    $ pip install djangorestframework-serializer-query-optimizations

Example
-------

It's all relatively simple. On the ``Meta`` class of your ``ModelSerializer``, define ``drf_serializer_query_optimizations.serializers.QueryOptimizingListSerializer`` as the ``list_serializer_class``:

.. code:: python

    from rest_framework.serializers import ModelSerializer
    from drf_serializer_query_optimizations.serializers import QueryOptimizingListSerializer

    from my_app import models


    class MySerializer(ModelSerializer):
        """A serializer for MyModel."""

        class Meta:
            """Meta class for the serializer."""

            model = models.MyModel
            fields = '__all__'
            list_serializer_class = QueryOptimizingListSerializer
            select_related = [
                'some_field',
                'another_field',
            ]
            prefetch_related = [
                'third_field',
                'fourth_field',
            ]

By this, the queryset of the serializer will be enhanced with ``select_related()`` and ``prefetch_related()``, which as arguments will be given the values of the model serializer ``Meta`` class attributes of the same names.

In this case, the queryset will be enhanced like this:

.. code:: python

    queryset = queryset.select_related(
        'some_field',
        'another_field'
    ).prefetch_related(
        'third_field',
        'fourth_field'
    )

If you want to see a "real" example, have a look at the models and serializers in `the example app <https://gitlab.com/gorgonzola/djangorestframework-serializer-query-optimizations/blob/master/example_app/serializers.py>`_ of this repository.


Further Reading on DRF (Query) Optimization
-------------------------------------------

In my search for ways to speed things up a bit, I found `this excellent blog post <https://www.dabapps.com/blog/api-performance-profiling-django-rest-framework/>`_ by Tom Christie (The author of DRF), in which he describes other ways to optimize your DRF based API.

I also found `this great blog post <https://ses4j.github.io/2015/11/23/optimizing-slow-django-rest-framework-performance/>`_ by Scott Stafford, which inspired me to make ``djangorestframework-serializer-query-optimizations``.

It suggests a somewhat standardized way to achieve the same as ``djangorestframework-serializer-query-optimizations`` does, by defining the query optimizations in a method on the serializer. This approach gives you a bit more flexibility, as you can do more or less whatever you want inside that method. On the other hand, you'll probably end up writing a lot of almost identical code for all your serializers. And you also have to remember to use the serializer method in your views if you want the benefits from the optimization.

With the approach in ``djangorestframework-serializer-query-optimizations``, you only need to set a custom list serializer and define which fields to use for query optimization. And you don't even need to touch your views, as it all happens automagically within the model serializer.
